package main

import (
	"fmt"
	"os"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	dir := "."
	port := "3000"

	args := os.Args
	for i := 0; i < len(args); i++ {
		if (args[i] == "--dir") {
			if (len(args) > i) {
				dir = args[i + 1]
			}
		} else if (args[i] == "--port") {
			if (len(args) > i) {
				port = args[i + 1]
			}
		}
	}

	app.Static("/", dir)

	println(fmt.Sprintf("Serving directory %s at port %s of localhost.\n", dir, port))

	app.Listen(fmt.Sprintf(":%s", port))
}
